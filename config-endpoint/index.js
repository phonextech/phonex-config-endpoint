const mysql = require("serverless-mysql")({
  config: {
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: "px_saas_dev",
  },
});

exports.handler = async (event, context) => {

  let final = { results: {} };

  try {

    const host = event.params.header.origin;
    const tenantId = event.params.header.tenant_id;

    /**
     * !!!
     * WE ARE GETTING THE TABLE NAME FROM THE REQUEST HEADER
     * SO IF WE DECIDE TO CREATE ANOTHER ENPOINT
     * WE JUST NEED TO MAKE SURE IT ENDS WITH THE TABLE NAME
     * Ex.: api/sass-config
     */
    let resource = event.context["resource-path"].split("/");
    resource = resource[resource.length - 1].replace("saas-", "");

    let sqlQuery, queryResults;
    
    if(!tenantId){
      
       sqlQuery = `SELECT * FROM ${resource}_table AS C WHERE C.tenant_id IN (SELECT tenant_id FROM router_table AS R WHERE R.host= ?)`;
  
       queryResults = await mysql.query({
        sql: sqlQuery,
        timeout: 5000,
        values: [host],
      });
    }
    else{
       sqlQuery = `SELECT * FROM ${resource}_table AS C WHERE C.tenant_id = ?`;
  
       queryResults = await mysql.query({
        sql: sqlQuery,
        timeout: 5000,
        values: [tenantId],
      });
    }

    await mysql.end();

    final = { results: {}, status: 200 };

    if (resource === 'i18n') {
      queryResults.forEach((entry) => {
        if (!final.results.hasOwnProperty(entry[`i18n_lang`])) {
          final.results[entry[`i18n_lang`]] = {}
        }
        final.results[entry[`i18n_lang`]][entry[`i18n_key`]] = {
          i18n_value: entry[`i18n_value`],
          i18n_value_plural: entry[`i18n_value_plural`],
          i18n_value_male: entry[`i18n_value_male`],
          i18n_value_female: entry[`i18n_value_female`]
        };
      });
    } else {
      queryResults.forEach((entry) => {
        final.results[entry[`${resource}_key`]] = entry[`${resource}_value`];
      });
    }

    return final;

  } catch (err) {

    await mysql.end();
    console.log('PhoneX API error:', err);
    final.status = 500;
    final.err = err;
    return final;

  }

};
