const mysql = require("serverless-mysql")({
  config: {
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: "px_saas_dev",
  },
});

exports.handler = async (event, context) => {

  let final = { results: {} };

  try {

     const tenantId = event.requestContext.authorizer.claims['custom:tenantId'];

    console.log('tenantId for contract check -> ' + tenantId);

    console.log('print events looking for tenant deails -> ' + JSON.stringify(event));

    // let sqlQuery, queryResults;
    
    
      //  sqlQuery = `SELECT * FROM tenant_contracts AS C WHERE C.tenant_id = ?`;
  
      //  queryResults = await mysql.query({
      //   sql: sqlQuery,
      //   timeout: 5000,
      //   values: [tenantId],
      // });

    // await mysql.end();

    final = { results: {}, status: 200 };

    // queryResults.forEach((entry) => {
      //TODO Transform the data to JSON
      // final.results[entry[`${resource}_key`]] = entry[`${resource}_value`];
      final.results = {
        "tenant_id": "T01",
        "contracts": [
          {
            "tersOfService": {
              "signed": false,
              "version": 1,
              "policy_id": "REV1.4",
              "JWT": "",
              "SignSourceIP": "",
              "URL": ""
            }
          },
          {
            "ApiLicenseAgreement": {
              "signed": false,
              "version": 1,
              "policy_id": "REV1.4",
              "JWT": "",
              "SignSourceIP": "",
              "URL": ""
            }
          },
          {
            "ApiLicenseAgreement": {
              "signed": false,
              "version": 1,
              "policy_id": "REV1.4",
              "JWT": "",
              "SignSourceIP": "",
              "URL": ""
            }
          },
          {
            "ExhibitForAgreedToFees": {
              "signed": false,
              "version": 1,
              "policy_id": "REV1.4",
              "JWT": "",
              "SignSourceIP": "",
              "URL": ""
            }
          }
        ]
      }
    // });

    let res =  {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      statusCode: 200,
      body: JSON.stringify(final),
      isBase64Encoded: true,
    };

    return res;

  } catch (err) {

    await mysql.end();
    console.log('PhoneX API error:', err);
    final.status = 500;
    final.err = err;
    return final;

  }

};
