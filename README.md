# PhoneX SAAS - Config Endpoint

Code for a AWS Lambda resource that will be used
PUBLICLY to provide the config data to the SAAS
application.

# aws commands
cd to config-endpoint
zip -r pxSaasEndpointConfigFn.zip .

aws lambda update-function-code --function-name px_saas_experience_config_endpoint --zip-file fileb://pxSaasEndpointConfigFn.zip --publish


